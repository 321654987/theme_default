@extends('layout')
@section('content')
<section class="site-content">
	<div class="container">
    	<div class="breadcum-area">
            <div class="breadcum-inner">
                <h3> Minha conta </h3>

                @include('customer.ol-profile')

            </div>

        </div>

        <div class="registration-area">
            
            
            <div class="row">     

                <div class="col-12 col-lg-3 spaceright-0">

                    @include('customer.sidebar')

                </div>

            	<div class="col-12 col-lg-9 new-customers">

                	<div class="col-12 spaceright-0">

                    	<div class="heading">
                            <h2> Meus dados </h2>
                            <hr>
                        </div>
                        
                         <div class="row">

                            <div class="col-sm-12">

                                @include('customer.form-profile')
                                                                
                                <h5 class="title-h5" style="margin-top:30px;"> Trocar senha </h5>

                                <hr class="featurette-divider">

                                @include('customer.form-password')

                            </div>

                        </div>
                        
                    </div>

                </div>

            </div>

        </div>
        		
    </div>
    
</section>

@endsection 	


