@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">

		<div class="breadcum-area">

				<div class="breadcum-inner">

					<h3> Login </h3>

					@include('system.login.ol')

				</div>

			</div>
			
		<div class="registration-area">

			<div class="heading">
				<h2>Entre ou crie uma conta				</h2>
				<hr>
			</div>

			<div class="row justify-content-center">

				<div class="col-12 col-md-6 col-lg-7 new-customers">

					<h5 class="title-h5"> Novo Cliente </h5>

					<p>Bem vindo, faço login ou cadastro agora mesmo</p>

					<hr class="featurette-divider">

					<p class="text-center"> Não tem conta? <a href="{{ URL::to('/signup')}}" class="btn btn-link ml-1"><b> Cadastra-se agora </b></a></p>
					
					@include('system.login.social')

				</div>

				<div class="col-12 col-md-6 col-lg-5 registered-customers">

					@include('system.login.alert')
					
					<h5 class="title-h5">
						Cadastre-se
					</h5>
					<p>Não tem conta? cadastre-se agora mesmo</p>

					@include('system.formlogin')

				</div>
			</div>
		</div> 
	</div>
</section>
	
@endsection 	


