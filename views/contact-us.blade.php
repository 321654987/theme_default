@extends('layout')
@section('content')

<section class="site-content">

    <div id="googleMap" style="width:100%;height:380px; margin-top:-30px; margin-bottom:30px; "></div>
    
	<div class="container">

  		<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3>Entre em contato conosco</h3>

                <ol class="breadcrumb">                    
                    <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">Inicio</a></li>
            		<li class="breadcrumb-item active">contato</li>
                </ol>

            </div>

        </div>

        <div class="contact-area">

        	<div class="heading">
                <h2>Entre em contato</h2>
                <hr>
            </div>

        	<div class="row">

                <div class="col-12 col-md-6 col-lg-8">

                	<p>Entre em contato</p>
                    
                    @include('system.formcontact')

                </div>
                
                <div class="col-12 col-md-6 col-lg-4">
                    
                    <ul class="contact-list">
                      <li> <i class="fa fa-map-marker"></i><span>{{$result['commonContent']['setting']->address}} {{$result['commonContent']['setting']->city}} {{$result['commonContent']['setting']->state}}, {{$result['commonContent']['setting']->zip}} {{$result['commonContent']['setting']->country}}</span> </li>
                      <li> <i class="fa fa-phone"></i><span>{{$result['commonContent']['setting']->phone_no}}</span> </li>
                      <li> <i class="fa fa-envelope"></i><span> <a href="mailto:{{$result['commonContent']['setting']->contact_us_email}}">{{$result['commonContent']['setting']->contact_us_email}}</a> </span> </li>
                    </ul>

                </div>

            </div>

        </div>

    </div>
    
</section>

@endsection 