@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">
   
  		<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3>Blog</h3>

                @include('news.ol')

            </div>

        </div>

        <div class="blog-area" style="margin-bottom:40px;">

            <form method="get" enctype="multipart/form-data" id="load_news_form">
                
                <input type="hidden"  name="category_id" value="{{ app('request')->input('category_id') }}">
                
                <div class="row">

                	 <div class="col-12 col-lg-3 spaceright-0">
                     	@include('news.sidebar')
                     </div>

                     <div class="col-12 col-lg-9">

                     	<div class="col-12 spaceright-0">

                        	<div class="row">

                            	<div class="toolbar mb-3">

                                    <div class="form-inline">

                                            @include('news.filter.grid')

                                            @include('news.filter.sort')

                                            @include('news.filter.perpage')

                                    </div>

                                </div>

                                @include('news.list')

                                @include('news.toobar')
                                                               

                            </div>

                        </div>                     	
                     	
                     </div>
                	
                </div>
                
             </form>

        </div>

    </div>
    
</section>

@endsection 	


