@extends('layout')
@section('content')
<section class="site-content">

	<div class="container">

		<div class="breadcum-area">

			<div class="breadcum-inner">

				<h3> Recuperar minha Senha </h3>

			</div>
		</div>
		<div class="registration-area">
			
			<div class="row justify-content-center">

				<div class="col-12 col-md-6 col-lg-7 new-customers">

					<h5 class="title-h4">Novo Cliente</h5>

					<p>Já tem cadastro, faço login agora mesmo</p>

					<hr class="featurette-divider">
					<p class="text-center"> Não tem conta? <a href="{{ URL::to('/signup')}}" class="btn btn-link ml-1"><b> Cadastra-se agora mesmo </b></a></p>
					
					<p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> Faça login:</p>

					<div class="row my-3 d-flex justify-content-center">
						<!--Facebook-->
						<a href="login/facebook" class="btn btn-light facebook"><i class="fa fa-facebook"></i> Entrar com Facebook </a>
						<!--Google +-->
						<a href="login/google" class="btn btn-light google"><i class="fa fa-google-plus"></i>Entrar com Google</a>
					</div>

				</div>

				<div class="col-12 col-md-6 col-lg-5 registered-customers">
					
					@if(Session::has('error'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							{!! session('error') !!}
							
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endif
					
					<h5 class="title-h4">
						Recuperar Senha
					</h5>

					<p> Por favor, digite seu e-mail para recuperar sua senha </p>

					@include('system.forgotpassword')

				</div>
			</div>
		</div> 
	</div>
</section>
	
@endsection 	


