@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">~

    	<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3> Endereço de entrega </h3>

                @include('customer.shipping.ol')

            </div>

        </div>

        <div class="my-shipping-area">
            
            <div class="row"> 

               	<div class="col-12 col-lg-3 spaceright-0">

                    @include('customer.sidebar')

                </div>

            	<div class="col-12 col-lg-9 my-shipping">

                	<div class="col-12 spaceright-0">

                        <div class="heading">
                            <h2>Endereços</h2>
                            <hr>
                        </div>

                        <div class="row">
                
                            @include('customer.shipping.list')

                        </div>
                    	
                        <hr class="featurette-divider">
                        
                        <div class="row">

                        	<div class="col-12">

                                <h5 class="title-h5">@if(!empty($result['editAddress'])) Salvar @else Salvar @endif </h5>
                                <hr class="featurette-divider">

                                @include('customer.shipping.add')

                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
            
        </div>

    </div>
    
</section>	
		
@endsection 	


