@extends('layout')

@section('content')

<section class="carousel-content">

    <div class="container">

        <div class="row">

            <div class="col-12 col-lg-3 p-0"> @include('home.box-categories') </div>
            
            <div class="col-12 col-lg-9 p-0"> @include('home.carousel') </div>
            
        </div>
        <div class="row " style="margin: 20px 0;  text-align: justify !important; ">
            <div class="col-md-4"> 
                <img class="img-responsive" src="{{$config[12]->value}}" alt="">
            </div>
            <div class="col-md-4"> 
                <img class="img-responsive" src="{{$config[13]->value}}" alt="">
            </div>
            <div class="col-md-4"> 
                <img class="img-responsive" src="{{$config[14]->value}}" alt="">
            </div>
        </div>
    
    </div>

</section>

<section class="site-content">

  <div class="container"> 

    <div class="products-area">

      <div class="row">

        <div class="col-md-12">

          @include('home.tab-content')

        </div>

      </div>

    </div>
   
  </div>

</section>

<section class="products-content"> 

    <div class="container-fuild">

        <div class="container">

            <div class="products-area"> 

                <div class="heading">
                    {{-- Mais vendidos --}}
                    <h2> {{$config[15]->value}} <small class="pull-right"><a href="{{ URL::to('/shop?type=topseller')}}" > Ver mais </a></small></h2>
                    <hr>
                
                </div>

                <div class="row"> 

                    <div class="col-xs-12 col-sm-12">
                        
                        @include('home.topseller')

                    </div>

                </div>

            </div>
                
        </div>
        
    </div>

    <div class="container-fuild">

        <div class="container">

            <div class="products-area"> 

                <div class="heading">
                    
                    <h2> Produtos especiais da semana  <small class="pull-right"><a href="{{ URL::to('/shop?type=special')}}" ></a></small></h2>
                    <hr>

                </div>

                <div class="row">         
                
                    <div class="col-xs-12 col-sm-12">

                        @include('home.categories')

                    </div>

                </div>

            </div>

        </div>
    
    </div>

    <div class="container-fuild">

        <div class="container">

            <div class="products-area"> 
    
                <div class="heading">

                    <h2> Categorias </h2>
                    <hr>

                </div>

                @include('home.categories')

            </div>

        </div>

    </div>

    <div class="container-fuild">

        <div class="container">

            <div class="products-area"> 

                <div class="heading">

                    <h2> Acabou de chegar <small class="pull-right"><a href="{{ URL::to('/shop')}}" >Ver mais</a></small></h2>
                    <hr>

                </div>

                <div class="row"> 

                    <div class="col-xs-12 col-sm-12">

                        @include('home.newest')

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@include('home.blog')

@endsection 