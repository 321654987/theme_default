<header id="header-area" class="header-area bg-primary">

	<div class="header-mini">

    	<div class="container">

            <div class="row align-items-center">

                <div class="col-12">
                
                	<nav id="navbar_0" class="navbar navbar-expand-md navbar-dark navbar-0 p-0">
                    
                    
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_collapse_0" aria-controls="navbar_collapse_0" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbar_collapse_0">

                            @include('header.ul')

                        </div>   

                    </div>
                    
                 </nav>

            </div>

        </div>

    </div>
    
    <div class="header-maxi">

    	<div class="container">

        	<div class="row align-items-center">

            	<div class="col-12 col-sm-12 col-lg-3 spaceright-0">

                    @include('header.logo')

                </div>
                
                <div class="col-12 col-sm-7 col-md-8 col-lg-6 px-0">     

                   @include('header.search')

                </div>
                
                <div class="col-12 col-sm-5 col-md-4 col-lg-3 spaceleft-0">

                    <ul class="top-right-list">
                    
                        
                        <li class="wishlist-header">

                            @include('header.wishlist')

                        </li>
                        
                        <li class="cart-header dropdown head-cart-content">

                            @include('header.cart')

                        </li>

                    </ul>

                </div>

            </div>

        </div>

    </div>

    <div class="header-navi">

    	<div class="container">

        	<div class="row align-items-center">
            
            	<div class="col-12">

                	<nav id="navbar_1" class="navbar navbar-expand-lg navbar-dark navbar-1 p-0 d-none d-lg-block">
                        
                        <div class="collapse navbar-collapse" id="navbar_collapse_1">
                        
                            <ul class="navbar-nav"> 

                                <li class="nav-item first"><a href="{{ URL::to('/')}}" class="nav-link"><i class="fa fa-home"></i></a></li>   

                                <li class="nav-item dropdown mega-dropdown open">

                                    <a href="javascript:void(0);" class="nav-link dropdown-toggle">
                                            Novidade na loja
                                    </a>
                            
                                    <ul class="dropdown-menu mega-dropdown-menu row" >

                                        <li class="col-sm-3">

                                            <ul>

                                                <li class="dropdown-header underline">Acabou de chegar</li>
                                                
                                                @if($result['commonContent']['recentProducts']['success']==1)

                                                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">

                                                    <div class="carousel-inner">
                                                        
                                                        @include('product.recentProduct');
                                                            
                                                    </div>
                                                    
                                                </div>

                                                @endif  

                                            </ul>

                                        </li>

                                        <li class="col-sm-9 pl-4 row">

                                            @include('home.categoriemenu'); 

                                        </li>   

                                    </ul>   

                                </li>



                                <li class="nav-item dropdown open">

                                    <a href="javascript:" class="nav-link dropdown-toggle"> Minhas Paginas </a>
                                
                                    <ul class="dropdown-menu">

                                        @include('home.pages');

                                    </ul>

                                </li>   
                                
                                
                                <li class="nav-item"> <a class="nav-link" href="{{ URL::to('/contact-us')}}"> Contato </a> </li>
                                
                                <li class="nav-item last">

                                    <a class="nav-link">
                                        <span>Entre em contato</span>(99 9999 9999)
                                    </a>

                                </li>
                            
                            </ul>

                        </div>

                    </nav>
                    
                    
                    <nav id="navbar_2" class="navbar navbar-expand-lg navbar-dark navbar-2 p-0 d-block d-lg-none">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_collapse_2" aria-controls="navbar_collapse_2" aria-expanded="false" aria-label="Toggle navigation"> Menu </button>
                        
                        <div class="collapse navbar-collapse" id="navbar_collapse_2">
                        
                            <ul class="navbar-nav"> 

                                <li class="nav-item first"><a href="{{ URL::to('/')}}" class="nav-link"><i class="fa fa-home"></i></a></li>  

                                <li class="nav-item"> <a class="nav-link" href="{{ URL::to('/shop')}}"> Loja </a> </li>
                                
                                <li class="nav-item dropdown mega-dropdown open">

                                    <div class="nav-link dropdown-toggle">
                                        Coleção
                                        <span class="badge badge-secondary"> Novos </span>
                                    </div>
                            
                                    <ul class="dropdown-menu mega-dropdown-menu row" >

                                        <li class="col-sm-3">

                                            <ul>

                                                <li class="dropdown-header underline"> Novidades para você </li>
                                                
                                                @include('home.recentProducts');
                                                
                                            </ul>

                                        </li>

                                        <li class="col-sm-9 pl-4 row">

                                            @foreach($result['commonContent']['categories'] as $categories_data)      

                                                <ul class="col-sm-4">                                            
                                                    <li class="dropdown-header"><a href="{{ URL::to('/shop')}}?category={{$categories_data->slug}}">{{$categories_data->name}}</a></li>
                                                    
                                                    @include('home.listcategorie');

                                                </ul>        

                                            @endforeach 

                                        </li>
                                        
                                    </ul>
                            
                                </li>

                               
                                <li class="nav-item dropdown open">

                                    <div class="nav-link dropdown-toggle"> Páginas </div>
                                
                                    <ul class="dropdown-menu">

                                        @include('home.listpages'); 

                                    </ul>

                                </li>
                                
                                <li class="nav-item"> <a class="nav-link" href="{{ URL::to('/contact-us')}}"> Contato </a> </li>

                                <li class="nav-item last">
                                    <a class="nav-link"><span> Entre em contato </span>({{$result['commonContent']['setting']->phone_no}})</a>
                                </li>
                            
                            </ul>

                        </div>

                    </nav>

                </div>

            </div>	

        </div>

    </div>
      
</header>



