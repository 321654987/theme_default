<section class="banner-content">

     <div class="container">

		<div class="row">

            <div class="col-12 col-md-6 col-lg-3 p-0">
                <div class="banner-single">
                    <div class="panel">
                        <h3 class="fa fa-truck"></h3>
                        <div class="block">
                            <h4 class="title">Frete oir nossa conta</h4>
                            <p>Em suas compras acima de R$ 199,00</p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-md-6 col-lg-3 p-0">
                <div class="banner-single">
                	<div class="panel">
                    	<h3 class="fa fa-barcode"></h3>
                        <div class="block">
                            <h4 class="title"> 10% de desconto </h4>
                            <p> Para pagamento com boleto </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3 p-0">
                <div class="banner-single second-last">
                	<div class="panel">
                        <h3 class="fa fa-life-ring"></h3>
                        <div class="block">
                            <h4 class="title"> Atendimento especializado  </h4>
                            <p>Dúvidas? entre em contato conosco</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3 p-0">
                <div class="banner-single last">
                    <div class="panel">
                    	<h3 class="fa fa-credit-card"></h3>
                        <div class="block">
                            <h4 class="title"> Pague em até 12x </h4>
                            <p>Parcele suas compras </p>
                        </div>
                    </div>
                </div>
            </div>
            
		</div>
        
    </div> 

</section>
