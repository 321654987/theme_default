<footer class="footer-content">

  <div class="container">

    <div class="row">

      <div class="col-12 col-md-6 col-lg-4">

        <div class="single-footer">

          <h5>Sobre nós</h5>
          <ul class="contact-list  pl-0 mb-0">

            <li> <i class="fa fa-map-marker"></i><span> {{$result['commonContent']['setting']->address}} {{$result['commonContent']['setting']->city}} {{$result['commonContent']['setting']->state}}, {{$result['commonContent']['setting']->zip}} {{$result['commonContent']['setting']->country}}</span> </li>
            <li> <i class="fa fa-phone"></i><span>      {{$result['commonContent']['setting']->phone_no}}</span> </li>
            <li> <i class="fa fa-envelope"></i><span> <a href="mailto:sales@brandbychoice.com">{{$result['commonContent']['setting']->contact_us_email}}</a> </span> </li>
      
          </ul>

        </div>

      </div>

      <div class="col-12 col-md-6 col-lg-4">

        <div class="footer-block">

        	<div class="single-footer">
            <h5>Serviços</h5>
            <ul class="links-list pl-0 mb-0">

              <li> <a href="{{ URL::to('/')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Inicio </a> </li>
              <li> <a href="{{ URL::to('/shop')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Loja </a> </li>
              <li> <a href="{{ URL::to('/orders')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meus Pedidos </a> </li>
              <li> <a href="{{ URL::to('/viewcart')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meu Carrinho </a> </li>            
              <li> <a href="{{ URL::to('/wishlist')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Meus Favoritos </a> </li>            
            
            </ul>
          </div>
          
          <div class="single-footer">
            <h5>Informações</h5>
            <ul class="links-list pl-0 mb-0">

                @include('home.pages')
              
              <li> <a href="{{ URL::to('/contact-us')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Entre em contato</a> </li>
            </ul>
          </div>

        </div>

      </div>
      
      <div class="col-12 col-lg-4">

        <div class="single-footer">

            <div class="newsletter">

            	<h5>Cadastre-se em nossa newsletter</h5>

                <div class="block">
                    <input type="email" placeholder="Sei email aqui">
                    <button type="button" class="btn btn-secondary">Increva-se</button>
                </div>

            </div>
            
            <div class="socials">

            	<h5>Redes Sociais</h5>

                <ul class="list">
                	  <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-skype"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                </ul>

            </div>
          
            <ul class="cat-list pl-0 mb-0">

		   		    @include('home.tagscategories')

            </ul> 
           
        </div>

      </div>

    </div>

  </div>
  
</footer>

<!--notification-->
<div id="message_content"></div>

<!--- loader content --->
<div class="loader" id="loader">
	<img src="{{asset('').'public/images/loader.gif'}}">
</div> 
