@extends('layout')

@section('content')

<section class="site-content">

    <div class="container">

        <div class="breadcum-area">

            <div class="breadcum-inner">

                <h3> Pedidos </h3>

                @include('customer.olorders')

            </div>

        </div>
        
        <div class="my-order-area">
        	
        	<div class="row">

            	<div class="col-12 col-lg-3 spaceright-0">

                        @include('customer.sidebar')

                </div>

            	<div class="col-12 col-lg-9 new-customers">
                	
                	<div class="col-12 spaceright-0">

                        <div class="heading">
                            <h2> Meus Pedidos </h2>
                            <hr>
                        </div>
                        
                        <div class="my-orders">
                            
                            @include('customer.alert')
                            
                        <div class="table-responsive">

                            @include('customer.orderslist')

                        </div>

                    </div>

                </div>	

            </div>    

        </div>

    </div>
    
</section>

@endsection 	


