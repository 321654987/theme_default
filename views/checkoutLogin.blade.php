@extends('layout')
@section('content')

<section class="site-content">
	
<div class="container">

	<div class="col-lg-12"><br>

	<ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="{{ URL::to('/')}}"> Inicio </a></li>
	  <li class="breadcrumb-item active">CheckOut</li>
	</ol> 
	<br>
	
		<h4 align="center"> Identifique-se </h4>
		<form name="signup" enctype="multipart/form-data" action="{{ URL::to('/processSignup')}}" method="post" align="center">
			
			<div class="form-row" align="center">

				<div class="col-md-2"></div>

				<div class="form-group col-md-8 ">
				  <label for="inputEmail4" class="col-form-label">Email</label>
				  <input style="text-align: center" type="email" class="form-control" id="inputEmail4" placeholder="email@provedor.com">
				</div>

			</div>
			
			<button type="submit" class="btn btn-primary " > Continuar </button>
			<p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> ou use suas redes sociais:</p>

			<div class="row my-3 d-flex justify-content-center">
				<!--Facebook-->
				<button type="button" class="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-facebook blue-text text-center"></i></button>

				<!--Google +-->
				<button type="button" class="btn btn-white btn-rounded z-depth-1a"><i class="fa fa-google-plus blue-text"></i></button>
			</div>
		</form>


		
	</div>
   </div>
 </section>	
		
@endsection 	


