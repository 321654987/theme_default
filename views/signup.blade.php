@extends('layout')

@section('content')

<section class="site-content">

<div class="container">

<div class="breadcum-area">

    <div class="breadcum-inner">

		<h3>Cadastre-se</h3>
		
        <ol class="breadcrumb">
            
            <li class="breadcrumb-item"><a href="{{ URL::to('/')}}"> Inicio </a></li>
            <li class="breadcrumb-item active">Cadastre-se</li>
		</ol>
		
	</div>
	
</div>

<div class="registration-area">

        <div class="heading">
            <h2> Cadastrar-me </h2>
            <hr>
		</div>
		
		<div class="row">
			
			<div class="col-2"></div>

			<div class="col-8 col-md-8 col-lg-8 new-customers">

				<h5 class="title-h5"> Informações pessoais </h5>

				<hr class="featurette-divider">

				@include('login.signup.alert')

				@include('login.signup.form')

			</div>

		</div>
	</div>		
	</div>
   </section>
		
@endsection 	


