
@extends('layout')
@section('content')

<section class="site-content">
	<div class="container">

        <div class="breadcum-area">

            <div class="breadcum-inner">

                <h3>CheckOut</h3>

                @include('checkout.ol')

            </div>

        </div>

		<div class="checkout-area">

            <div class="row">

				<div class="col-12 col-lg-8 checkout-left">

                    <ul class="nav nav-pills" id="pills-tab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==0) active @elseif(session('step')>0) active-check @endif" id="shipping-tab" data-toggle="pill" href="#pills-shipping" role="tab" aria-controls="pills-shpping" aria-expanded="true">Endereço de entrega</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==1) active @elseif(session('step')>1) active-check @endif" @if(session('step')>=1) id="billing-tab" data-toggle="pill" href="#pills-billing" role="tab" aria-controls="pills-billing" aria-expanded="true" @endif > Endereço de cobrança </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==2) active @elseif(session('step')>2) active-check @endif"  @if(session('step')>=2)  id="shipping-methods-tab" data-toggle="pill" href="#pills-shipping-methods" role="tab" aria-controls="pills-shipping-methods" aria-expanded="true"  @endif> Opções de envio </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==3) active @elseif(session('step')>3) active-check @endif"  @if(session('step')>=3)  id="order-tab" data-toggle="pill" href="#pills-order" role="tab" aria-controls="pills-order" aria-expanded="true"  @endif> Detalhes do Pedido </a>
                        </li>

                    </ul>
                    
                    <div class="tab-content" id="pills-tabContent">

                        <div class="tab-pane fade @if(session('step') == 0) show active @endif" id="pills-shipping" role="tabpanel" aria-labelledby="shipping-tab">

                            @include('checkout.shipping')

                        </div>
                        
                        <div class="tab-pane fade @if(session('step') == 1) show active @endif" id="pills-billing" role="tabpanel" aria-labelledby="billing-tab">

                            @include('checkout.billing')

                        </div>
                        
                        <div class="tab-pane fade @if(session('step') == 2) show active @endif" id="pills-shipping-methods" role="tabpanel" aria-labelledby="shipping-methods-tab">
                            
                            <div class="shipping-methods">

                                <p class="title"> Selecione um método de envio preferido para usar neste pedido.                                </p>

                                @include('checkout.freight')

                            </div>

                        </div>
                        
                        {{-- //pagamento --}}
                        <div class="tab-pane fade @if(session('step') == 3) show active @endif" id="pills-order" role="tabpanel" aria-labelledby="order-tab"> 
                            
                            <div class="col-12 col-lg-12 ">    

                                <div class="payment-area">

                                    <div class="heading">
                                        <h2>Como quer pagar?</h2>
                                        <hr>
                                    </div>

                                    <div class="payment-methods">
                                        <p class="title"> Escolha como quer pagar </p>
                                        
                                        <div class="alert alert-danger error_payment" style="display:none" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            Por favor, selecione o seu método de pagamento
                                        </div>	
                                        <style>
                                            #tabPayment a{
                                                border: 1px solid #ccc;
                                            }
                                            #tabPayment a{
                                                border-right: 1px solid #ccc;
                                            }
                                            #tabPayment a.active{
                                                border-bottom: 1px solid #fff !important;
                                            }
                                        </style>

                                        @include('checkout.payment')

                                    </div>
                                    
                                </div>

                            </div>

                            <div class="order-review">

                                <div class="table-responsive">

                                        @include('checkout.cart.list')

                                </div>                   
                               
                            </div>

                            <div class="notes-summary-area">
                                <div class="heading">
                                    <h2> Quer deixar alguma observação no seu pedido? </h2>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 order-notes">

                                        <p class="title">Alguma Observação?</p>

                                        @include('checkout.notes')

                                    </div>

                                </div>
                            </div>
                        
                        </div>

                    </div>
                        
                </div> <!--CHECKOUT LEFT CLOSE-->
                
                <div class="col-12 col-lg-4 checkout-right">   

                    <div class="order-summary-outer">

                    	<div class="order-summary">

                            <div class="table-responsive">

                                <table class="table">

                                	<thead>
                                    	<tr>
                                        	<th colspan="2"> Resumo do pedido </th>
                                        </tr>
                                    </thead>

                                  	<tbody>
                                          
                                            @include('checkout.summary.subtotal')
                                            @include('checkout.summary.costshipping')
                                            @include('checkout.summary.discount')
                                            @include('checkout.summary.total')
                                                                               
                                    </tbody>
                                    
                                </table>

                            </div>

                        </div> 

                        @include('checkout.summary.codecoupon')

                    </div>	

                </div>	<!--CHECKOUT RIGHT CLOSE-->
                
            </div>
		</div>
	</div>
</section>

@endsection 	


