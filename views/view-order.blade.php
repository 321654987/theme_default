@extends('layout')

@section('content')

<section class="site-content">
    
    <div class="container">

        <div class="breadcum-area">

            <div class="breadcum-inner">

                <h3> Meus pedidos </h3>

                @include('customer.order.detail.ol')

            </div>

        </div>

        <div class="orders-detail-area">

            <div class="row">

            	<div class="col-12 col-lg-3 spaceright-0">
                    @include('customer.sidebar')
                </div>

                <div class="col-12 col-lg-9">

                    <div class="col-12 spaceright-0">

                        <div class="heading">
                            <h2> Informações do Pedido </h2>
                            <hr>
                        </div>

                        <div class="row">

                            <div class="col-12 col-md-6 col-lg-6 card-box">

                                @include('customer.order.detail.card-01')

                            </div>
                            <div class="col-12 col-md-6 col-lg-6 card-box">
                                
                                @include('customer.order.detail.card-02')

                            </div>
                            <div class="col-12 col-md-6 col-lg-6 card-box">

                                 @include('customer.order.detail.card-03')

                            </div>
                            <div class="col-12 col-md-6 col-lg-6 card-box">

                                @include('customer.order.detail.card-04')

                            </div>
                        </div>
                        <div class="row align-items-start">
                            <div class="col-12 col-md-6 col-lg-8">
                                <div class="table-responsive">

                                @include('customer.order.detail.list-item')

                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">

                                @include('customer.order.detail.summary')

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">

                                @include('customer.order.detail.comment')

                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
 </section>		
@endsection 	


