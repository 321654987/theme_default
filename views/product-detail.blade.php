@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">

		<div class="breadcum-area">

        	<div class="breadcum-inner"> 

                <h3>{{$result['detail']['product_data'][0]->products_name}}</h3>
                
                @include('product.ol')

            </div>
            
		</div>

		<div class="product-detail-area">

			<div class="row">

				<div class="col-12">

                	<div class="detail-area">

                        <div class="row">

                            <div class="col-12 col-lg-7">

                                @include('product.slideimage')

                            </div>
            
                            <div class="col-12 col-lg-5">

                                <div class="product-summary">

                                    <div class="like-box">

                                        <span products_id='{{$result['detail']['product_data'][0]->products_id}}' class="fa @if($result['detail']['product_data'][0]->isLiked==1) fa-heart @else fa-heart-o @endif is_liked">
                                        	<span class="badge badge-secondary">{{$result['detail']['product_data'][0]->products_liked}}</span>
                                        </span>     

                                    </div>        

                                    <h3 class="product-title">{{$result['detail']['product_data'][0]->products_name}}</h3> 

                                    @include('product.info')

                                    @include('product.price')
        
                                    @include('product.options')

                                </div>	 

                            </div>
                            
                            <div class="col-12">

                                <div class="product-tabs">
                                    
                                    <ul class="nav nav-pills" id="myTab" role="tablist">

                                        <li class="nav-item">

                                            <a class="nav-link active" id="product-desc-tab" data-toggle="tab" href="#product_desc" role="tab" aria-controls="product_desc" aria-selected="true"> Descrição do produto </a>
                                     
                                        </li>
                                      
                                    </ul>
                                                                        
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="product_desc" role="tabpanel" aria-labelledby="product-desc-tab">

                                                @include('product.description')

                                        </div>

                                    </div>
                                    
                                </div>

                            </div>

                        </div>

                    </div>
				
                    <div class="related-product-area">

                        <div class="heading">

                            <h2> Produtos Relacionados <small class="pull-right"><a href="{{ URL::to('/shop?category_id='.$result['detail']['product_data'][0]->categories_id)}}">Ver mais</a></small></h2>
                            <hr>

                        </div>

                        @include('product.related')
    
                        </div>

                    </div>

                </div>

            </div>
            
        </div>
        
    </div>
    
</section>

@endsection




