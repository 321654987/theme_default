@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">

  		<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3> Meus favoritos </h3>

            </div>

        </div>

    	<div class="shop-area">
            
        	<form method="get" enctype="multipart/form-data" id="load_wishlist_form" style="width:100%;">
            
            <input type="hidden"  name="search" value="{{ app('request')->input('search') }}">
            
            <input type="hidden"  name="category_id" value="{{ app('request')->input('category_id') }}">
            
            <input type="hidden"  name="load_wishlist" value="1">
            
            <input type="hidden"  name="type" value="wishlist">
            
        	<div class="row">

            	<div class="col-12 col-lg-3 spaceright-0">

                    @include('customer.sidebar')

                </div>

            	<div class="col-12 col-lg-9 new-customers">
                	
                	<div class="col-12 spaceright-0">

                        <div class="heading">
                            <h2>Favoritos</h2>
                            <hr>
                        </div>

                        <div class="row">

                        	@if($result['products']['success']==1)

                            <div class="toolbar mb-3 loaded_content">

                                <div class="form-inline">
                                    <div class="form-group col-12 col-md-4">
                                        <label class="col-12 col-lg-5 col-form-label">Mostrar</label>

                                       @include('customer.wishlist.grid')

                                    </div>

                                    <div class="form-group col-12 col-md-4"></div>

                                    <div class="form-group col-12 col-md-4">
                                        <label class="col-12 col-lg-4 col-form-label">Limite</label>

                                        @include('customer.wishlist.sort')


                                        <label class="col-12 col-lg-5 col-form-label">Por pagina</label>
                                    </div>

                                </div>

                            </div>
                            
                                @include('customer.wishlist.list-item')

                                @include('customer.wishlist.toolbar')
                           
                           @endif

                           <div id="loaded_content_empty" @if($result['products']['success']==1) style="display: none;" @endif>
                           		Nenhum produto em sua lista de favoritos
                           </div>

                        </div>

                    </div>  

                </div>

        	</div>

            </form>

		</div>

    </div>

</section>

@endsection 