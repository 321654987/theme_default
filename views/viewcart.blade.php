@extends('layout')

@section('content')

<section class="site-content">

    <div class="container">

        <div class="breadcum-area">

            <div class="breadcum-inner">

                <h3> Carrinho </h3>

            </div>

        </div>

        <div class="cart-area">

            <div class="row">
                 <?php $price = 0; ?>
                 
				@if( empty($result['cart']) == false )
                     
                <div class="col-12 col-lg-8 cart-left">
                    
                    <div class="row">

                        @include('checkout.cart.form-list')
                        
                    </div>
                        
                    <div class="row button">

                        <div class="col-12 col-sm-6">     

                            <div class="row">
                            	<a href="{{ URL::to('/shop')}}" class="btn btn-dark"> Comprar Mais </a>
                            </div>

                        </div>

                        <div class="col-12 col-sm-6">

                        	<div class="row justify-content-end">
                            	<button class="btn btn-dark" id="update_cart"> Atualizar Carrinho </button>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-12 col-lg-4 cart-right">

                	<div class="order-summary-outer">

                    	<div class="order-summary">

                            <div class="table-responsive">

                                <table class="table">

                                	<thead>

                                    	<tr>
                                        	<th align="left" colspan="2"> Resumo do pedido </th>
                                        </tr>

                                    </thead>

                                  	<tbody>

                                        @include('checkout.summary.subtotal')
                                        @include('checkout.summary.costshipping')
                                        @include('checkout.summary.discount')
                                        @include('checkout.summary.total')
                                                                               
                                    </tbody>
                                    
                                </table>

                            </div>

                        </div>     

                        @include('checkout.summary.codecoupon')
                        
                        <div class="buttons">

                            <a href="{{ URL::to('/checkout')}}" class="btn btn-block btn-secondary" > Finalizar compra </a>
                            
                        </div>

                    </div>

                </div>
                
                @else
                
                <div class="col-xs-12 col-sm-12 page-empty">

                    <span class="fa fa-cart-arrow-down"></span>
                    
                	<div class="page-empty-content">
                    	<span> Carrinho Vazio </span>
                    </div>

                </div>

               @endif	

            </div>
            	
        </div>
        
    </div>
    
 </section>
		
@endsection 	


