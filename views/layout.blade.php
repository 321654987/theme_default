<!doctype html>
  <html>

    @include('system.meta')
    
    @include('common.head')

  <body>

      @include('common.header')

      @yield('content')
      
      @include('common.highlights')
        
      @include('common.footer')

      @include('home.ftsystem')

      @include('system.scripts')
      
      @if(!empty($result['commonContent']['setting']->end_body_tag))
        <?=stripslashes($result['commonContent']['setting']->end_body_tag)?>
      @endif
  
  </body>

</html>

