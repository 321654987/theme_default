@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">

    	<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3> Detalhes da notícia   </h3>

                @include('news.detail.ol')

            </div>

        </div>

        <div class="blog-area">

            <div class="row">

            	<div class="col-12 col-lg-3 spaceright-0">
                    
                    @include('news.sidebar')

                 </div>

                 <div class="col-12 col-lg-9">

                 	<div class="col-12 spaceright-0">

                    	<div class="row">

                            <div class="blogs blogs-detail" style="padding-left:0;">    
                                    
                                <div class="blog-post">

                                    <article>

                                      @include('news.detail.thumb')

                                      @include('news.detail.text')
                                        
                                      

                                    </article>

                                </div> 

                             </div>	

                        </div>

                    </div>

                 </div>

            </div>		

        </div>

    </div>
    
</section>

@endsection 	


