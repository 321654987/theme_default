@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">

  		<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3><?=$result['pages'][0]->name?></h3>

                @include('page.ol')

            </div>

        </div>

        <div class="info-pages-area">
        	
            <div class="row">
            	
            	<div class="col-12 col-lg-9">
                	
                	<div class="col-12 spaceleft-0">

                        @include('page.content')

                    </div>

                </div>
                
                <div class="col-12 col-lg-3 spaceleft-0">
                    @include('page.sidebar')
                 </div>

            </div>

        </div>

    </div>
    
</section>

@endsection 