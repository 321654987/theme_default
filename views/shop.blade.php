@extends('layout')

@section('content')

<section class="site-content">

	<div class="container">

    	<div class="breadcum-area">

            <div class="breadcum-inner">

                <h3>Loja</h3>

                @include('shop.ol')

            </div>

        </div>

		<div class="shop-area">

            <form method="get" enctype="multipart/form-data" id="load_products_form">

                @include('shop.searchresult')
                
                <div class="row">   

                    <div class="col-12 col-lg-3 spaceright-0">

                        @include('shop.sidebar_shop')

                    </div>
                    
       
                    <div class="col-12 col-lg-9">

                        <div class="col-12 spaceright-0">

                        	<div class="row">
                            
                            @include('shop.resultsearchh4')
                            
                             @if($result['products']['total_record']>0)

                            	<div class="shop-banner mb-3">

                                    <img class="img-fluid" src="{!! asset('public/images/category_img.jpg') !!}" alt="banner">

                                </div>
                                
                                <div class="toolbar mb-3">

                                    <div class="form-inline">

                                        <div class="form-group col-12 col-md-4">

                                            <label class="col-12 col-lg-5 col-form-label">Mostrar</label>

                                            <div class="col-12 col-lg-7 btn-group">

                                            @include('shop.display')
                                                
                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-4 center">

                                            <label class="col-12 col-lg-4 col-form-label">Ordernar</label>

                                            @include('shop.sort')

                                        </div>              

                                        <div class="form-group col-12 col-md-4">

                                            <label class="col-12 col-lg-4 col-form-label">Limite</label>

                                            @include('shop.perpage')

                                            <label class="col-12 col-lg-5 col-form-label">Por pagina</label>

                                        </div>

                                    </div>

                                </div>
                                
                                @include('shop.products-3x')

                                @include('shop.loadmore')
                                
                                @elseif(empty(app('request')->input('search')))
                                
                                    <p>Nenhum produto encontrato</p>

                                @endif  

                            </div>

                        </div>
                        
                    </div>
                    
                                        
                </div>
                
            </form>
            
        </div>
        
    </div>
    
</section>

@endsection 